# How To: Set Up WillNET VPN File Sharing on Windows

This guide walks you through how to set up the VPN File Sharing mechanism through the Windows built-in Mapped Network Drives subsystem.

---

## Prerequisites

1. Connection to WillNET VPN. (**_[See guide](HOWTO_Setup-VPN.md)_**)

---

### Setup

1. In File Explorer, click on **"This PC"** and click **"Map network drive"** in the top banner.

    ![Map network drive.](media/pic3.png "Map network drive")

2. **Choose a Drive letter**, or leave it as the default "Z:"; it doesn't matter.

    - In the "Folder" field type: **\\\\conduit.lab.willstechblog.ddns.net\\vpn** (**_Note:** Make sure to use_ "\\" _and not_ "/")

    - Make sure **"Reconnect at sign-in"** and **"Connect using different credentials"** are _checked_.

    - Click **Finish**.

    ![Set up network drive.](media/pic4.png "Set up network drive")

3. Enter your VPN login credentials:

    ![Login.](media/pic5.png "Login")

Now you're in! And would ya look at that - **_seamlessly integrated into Windows_** so it's **_super freaking easy_** (and I'm **_super freaking awesome_** 😁)

Now this network share will appear in File Explorer under Network Locations right below your local hard drives:

![Network Locations.](media/pic6.png "Network Locations")

---

## Important Notes

- Your personal folder can _only_ be accessed and modified by you, but the **SHARED** folder can be accessed **_and modified_** by anyone. So if you post a beautiful selfie in the **SHARED** folder, someone can come along and draw a penis on your forehead and overwrite your original file, and then everyone would think you're gay for having a penis on your forehead you gayrod...

- Currently there is **200 GB** of storage available across all users and the shared folder, but I'm planning on expanding it to **4 TB** Real Soon.

---

## Other Guides

- **[How To: Install/Configure Pidgin Messenger](HOWTO_Setup-Pidgin.md)**