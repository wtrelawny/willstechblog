# How To: Set Up WillNET VPN Client

This is the initial guide for getting started with WillNET VPN. There are links to the guide for configuring various VPN services at the end.

---

## Prerequisites

1. Send me your **email address**. This will be used to keep you up to date on VPN changes and service outages.

    - Let me know if you're on Discord, I'll invite you to my dedicated VPN channel for faster updates, help & troubleshooting, and general discussions.

---

### Install Certificates

1. Install the [WebAdmin certificate](media/WebAdmin.cer) for the site (click "**_View raw_**" after following the link).

    - Open the WebAdmin.per file, accept file warning.

    - Click **"Install Certificate..."**:

    ![Install Certificate.](media/pic7.png)

    - Choose **"Local Machine"**, click Next.

    - Install the cert into the **"Trusted Root Certification Authorities"** Certificate store:

    ![Install to Trusted Root Certificate Authorities store.](media/pic8.png)

    - Click **"Finish"**

### Configure VPN Client

1. **Download** the OpenVPN installer for Windows: [OpenVPN Client v2.4.6](https://swupdate.openvpn.org/community/releases/openvpn-install-2.4.6-I602.exe)
2. Launch the **OpenVPN GUI** program, right click its icon in the system tray, and click **"Import File."**
3. **Import** the config file I emailed you. Then right-click the OpenVPN tray icon again and click **"Connect."**
4. When prompted for login, enter the **credentials** I sent you in the config email.

## Change User Password

1. Go to the **_Portal Login URL_** provided in the email, and log in with the provided user credentials:

    ![Log in.](media/pic1.png "Log in")  

2. Go to Change Password:

    ![Change password.](media/pic2.png "Change password")  

3. Drink a beer! 🍻

Hope you guys enjoy! I'll keep you posted on new developments and services I roll out.

---

## Next Steps

- **[How To: Set Up WillNET VPN File Sharing on Windows](HOWTO_Setup-FileSharing.md)**

- **[How To: Install/Configure Pidgin Messenger](HOWTO_Setup-Pidgin.md)**

---

## Service Plans

- Minecraft server (possibly hosted at Kohly's new townhouse just for the fun of added network complexity! 😁)
- ~~Chat server (again [because it was super awesome last time and we should use it more])~~ -- **COMPLETED**
- ~~File sharing (a LOT better implementation than last time with the clunky WinSCP client and no thumbnails)~~ -- **COMPLETED**
- Email (just for lots of shits and probably very few giggles... and much hair torn out)
- Build web front-end for conduit with various fun stuff. -- _IN PROGRESS_

-WT