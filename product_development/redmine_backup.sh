#!/bin/bash
# redmine server full backup
# by: william trelawny
# 11/18/2017
# ref: https://docs.bitnami.com/installer/apps/redmine/#backup-on-linux-and-mac-os-x
# ref: https://unix.stackexchange.com/questions/127352/specify-identity-file-id-rsa-with-rsync

# use the following /etc/crontab entry (less the # ofc, smartass) to run every night at midnight:
# ref: https://serverfault.com/questions/137468/better-logging-for-cronjobs-send-cron-output-to-syslog
# 0 0 * * * root /usr/local/sbin/redmine_backup.sh 2>&1 | /usr/bin/logger -t redmine_backup

# stop all redmine services
cd  /opt/redmine-*
REDMINE_DIR=$(pwd)
./ctlscript.sh stop

# create backup
cd /root
tar -pczvf /root/"redmine-backup_$(date +%Y-%m-%d_%H:%M:%S).tar.gz" "${REDMINE_DIR}/"

# export backup to CORE.lab.omni
rsync -Pav --remove-source-files -e "ssh -i /home/mithos/.ssh/PROD_mithos-id_rsa" /root/redmine-backup_*.tar.gz mithos@10.0.0.1:~/backups/

# restart redmine services
"${REDMINE_DIR}"/ctlscript.sh start