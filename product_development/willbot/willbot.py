"""
Author:         William Trelawny
Date:           7/31/2018
Last Updated:   8/4/2018
Name:           WillBOT - Discord bot
"""

"""
References:

discord.py v1.0.0a ("Rewrite") Main Page: https://discordpy.readthedocs.io/en/rewrite/index.html
discord.py Rewrite Installation: https://discordpy.readthedocs.io/en/rewrite/intro.html
discord.py v0.16 "async" vs. v1.0 "Rewrite": https://discordpy.readthedocs.io/en/rewrite/migrating.html
discord.py Logging setup: https://discordpy.readthedocs.io/en/rewrite/logging.html
discord.py Rewrite API: https://discordpy.readthedocs.io/en/rewrite/api.html#
discord.ext.commands.Bot() API: https://discordpy.readthedocs.io/en/rewrite/ext/commands/api.html#
discord.ext.commands.Bot() Commands List: https://discordpy.readthedocs.io/en/rewrite/ext/commands/commands.html
discord.py Rewrite FAQ: https://discordpy.readthedocs.io/en/rewrite/faq.html
Error handling module: https://gist.github.com/EvieePy/7822af90858ef65012ea500bcecf1612
Background_task example: https://github.com/Rapptz/discord.py/blob/rewrite/examples/background_task.py
"""

"""
Import libraries
"""

import discord
from discord.ext import commands
import logging
import time
import datetime as dt
import asyncio
import os


willbot = commands.Bot(command_prefix='!', description='A simple-minded little bot.', pm_help=True)

"""
Configure logging:
"""

def bot_logger():
    global logger
    logger = logging.getLogger('discord')
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler(filename='willbot_{0}.log'.format(dt.datetime.utcnow().strftime('%Y-%m-%d_%H-%M')), encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s : %(levelname)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
    logging.Formatter.converter = time.gmtime
    logger.addHandler(handler)
    logger.debug('Configured logging.')


"""
Error handling:
"""

@willbot.event
async def on_error(event, *args, **kwargs):
    logger.warning(event)
    logger.warning(args[0])

@willbot.event
async def on_command_error(ctx, exc):
    logger.warning(exc)


"""
Setup
"""

@willbot.event
async def on_ready():
    logger.info('Logged in as: {0} ({1})'.format(willbot.user.name, willbot.user.id))

### commenting out b/c it's low priority right now, may revisit
# @willbot.event
# async def to_background(self):
#         await self.wait_until_ready()
#         counter = 0
#         channel = self.get_channel(1234567) # channel ID goes here
#         while not self.is_closed():
#             counter += 1
#             await channel.send(counter)
#             await asyncio.sleep(60) # task runs every 60 seconds


"""
Callbacks (events/commands)
"""

# Close the bot
@willbot.command()
async def exit(ctx):
    await willbot.close()


"""
Music Player
"""



# Add function
@willbot.command()
async def add(ctx, num1: int, num2: int):
    await willbot.get_channel(424408326483345419).send(num1 + num2)

Play YouTube links:
@willbot.command(pass_context=True)
async def yt(ctx, url):
    author = ctx.message.author
    channel = message.

"""
Main
"""

def main():
    bot_logger()
    willbot.run('NDc0MjM5NDQ5OTQ2MTI4NDEz.DkNm-w.LlscUEs8rZEMgORwD8g9Nt4PD1o')


main()