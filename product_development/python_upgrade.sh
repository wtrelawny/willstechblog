#!/bin/bash

# title: Python_Upgrade
# desc: Fetches & installs specified version of Python
# author: william trelawny
# date created: 4/3/2018
# last updated:
# ref: https://raspberrypi.stackexchange.com/questions/59381/how-do-i-update-my-rpi3-to-python-3-6

# set working dir to /tmp
cd /tmp

# collect package version number
echo "Enter python version to install: "
read python_ver
python_pkg="Python-${python_ver}"

# Download gzipped tarball
wget https://www.python.org/ftp/python/${python_ver}/${python_pkg}.tgz

# Extract package
tar xzvf ${python_pkg}.tgz

# Install package
cd /tmp/${python_pkg}/
./configure
make
make install
