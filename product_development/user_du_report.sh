#!/bin/bash
##################################
# User Disk Usage Report                     
# created by: will               
# date: 5/10/2018                  
# last modified: 5/10/2018         
# purpose: Generate report on
#    disk usage per user          
# calls: cd, du, chown, sed,
#   scp, sshpass                
# called by: root 		         
##################################

OF="/home/mithos/User Disk Usage Report_$(date +"%Y-%m-%d").csv"

# set working dir
cd /home

# list disk usage for each user home folder, output to mithos home dir
# du : estimate file space usage
# -c : produce a grand total at the bottom
# -h : print sizes in human-readable format
# -s : summarize, display total only for each arg (no subdirs)
# -x : skip dirs on different filesystems (to handle shared mounted from /dev/sdb1)
du -schx * > "${OF}"

# change owner of report to mithos
chown mithos:mithos "${OF}"

# convert to csv format (replace "\t" tab chars with ",")
# add column headers to file
sed -i 's/\t/,/g; 1s/^/usage,user\n/' "${OF}"

# scp file to Falco, use sshpass to pass Falco's pw to scp command
sshpass -f /home/mithos/.ssh/.falco_pass scp "${OF}" falco@192.168.0.10:'"D:\Users\Falco\Google Drive\Documents\Computer_Stuff\Home_Lab\conduit\User_Disk_Usage_Reports"'
