#!/bin/bash
##################################
# adduser.sh                     
# created by: will               
# date: 8/31/17                  
# last modified: 8/13/2019         
# purpose: Create new user and do
#   other setup stuff done automagically.
#   Like ssh keypairs, chroot, etc.          
# calls: sudo, adduser,	usermod,	     
#	ssh-keygen,	mkdir, chmod, chown,
#   touch, echo, cat, rm, mount                
# called by: root 		         
##################################

###############
#  Init vars  
###############

# Save distro name to var RELEASE; strip off " chars, drop first 12 chars of line, take only first word
# SUUUPER sloppy, I fucking know... will clean up when I have more time
RELEASE="$(cat /etc/*release | grep "PRETTY_NAME" | sed -r 's/"//g;s/^.{12}//' | cut -d" " -f1)"
HOSTNAME="$(hostname -f)"

# Accept command args for username and ssh key size
USERNAME=$1
KEYSIZE=$2

##############
#    Main    
##############

# prompt for username of new user
main() {
    # If username not provided as command arg, prompt for it:
    if [ -z $USERNAME ];
    then
        read -p "Enter username: " USERNAME
    fi
    VPNDIR="/home/vpn"
    USERHOME=""${VPNDIR}"/"${USERNAME}""
    WD=""${VPNDIR}"/"${USERNAME}"/.ssh"

    # prompt for FTP-only user **ONLY** if running script on conduit.lab.omni
    if [ "${HOSTNAME}" == "conduit.lab.willstechblog.ddns.net" ];
    then
        read -p "Is this an FTP-only user on conduit.lab.willstechblog.ddns.net? [y/n] " FTP
        FTP=$(echo "${FTP}" | tr '[:upper:]' '[:lower:]' | cut -c1);
    else
        FTP="n";
    fi

    # prompt for sudoer user
    read -p "Sudo user? [y/n] " SUDO
    SUDO=$(echo "${SUDO}" | tr '[:upper:]' '[:lower:]' | cut -c1)

    # prompt for key size for new user's ssh key, generate keypair
    if [ -z $KEYSIZE ];
    then
        read -p "Enter ssh key size (2048 | 4096 | 8192): " KEYSIZE
    fi

    # handling adduser command across multiple platforms, inc. debian and rhel.
    # debian adduser command incl. gecos options, rhel does not
    if [ "${RELEASE}" = "Ubuntu" ] || [ "${RELEASE}" = "Raspbian" ];
    then
        adduser "${USERNAME}" --gecos " , , , " --disabled-password --home "${USERHOME}"
        conf_user
    elif [[ "${RELEASE}" = "CentOS" ]]
    then
        adduser "${USERNAME}";
        conf_user
    else
        printf "This script does not support this Linux distro at this time."
        printf "Supported distros: Ubuntu, Raspbian, CentOS."
        bye
    fi
}

conf_user() {
    # if group vpnusers exists, add user to group. If not, create it then add user to group
    if grep -q vpnusers /etc/group
    then
        usermod -aG vpnusers "${USERNAME}";
    else
        addgroup vpnusers
        usermod -aG vpnusers "${USERNAME}";
    fi

    # if user is sudoer, add to sudo group
    if [ "${SUDO}" = "y" ];
    then
        usermod -aG sudo "${USERNAME}";
    fi

    # create .ssh dir under user home
    sudo -u "${USERNAME}" mkdir "${USERHOME}"/.ssh
    # generate ssh keypair
    sudo -u "${USERNAME}" ssh-keygen -t rsa -b $KEYSIZE -C ""${USERNAME}"@"${HOSTNAME}"" -f ""${WD}"/${USERNAME}-id_rsa" -N ""
    # create authorized_keys file under ~/.ssh
    sudo -u "${USERNAME}" touch "${WD}"/authorized_keys
    # add comment for key to authorized_keys
    sudo -u "${USERNAME}" echo -e "# ${USERNAME} pubkey" >> "${WD}"/authorized_keys
    # append user pubkey to authorized_keys
    sudo -u "${USERNAME}" cat "${WD}"/"${USERNAME}"-id_rsa.pub >> "${WD}"/authorized_keys
    # set .ssh dir perms to user-only rwx
    sudo -u "${USERNAME}" chmod 700 "${USERHOME}"/.ssh
    # set .ssh dir contents to user-only read-only
    sudo -u "${USERNAME}" chmod 400 "${USERHOME}"/.ssh/*


    if [ "${FTP}" = "y" ];
    then
        passwd -d "${USERNAME}"
        chrooting
    else
        chage -d 0 "${USERNAME}"
        bye
    fi
}

chrooting() {
    # Set up chroot jail
    echo "Setting up chroot jail..."
    # Disable shell for user
    usermod -s /bin/false "${USERNAME}"
    echo "Disabled shell for ${USERNAME}."
    # Set user chroot subdirs ownership to user.user
    chown -R "${USERNAME}".vpnusers "${USERHOME}"
    # Set user chroot root dir ownership to root.vpnusers
    chown root.vpnusers "${USERHOME}"
    # Set perms on user chroot to user-only rwx
    chmod 750 "${USERHOME}"
    echo "Set ownership/perms on ${USERHOME} and subdirectories."
    # Make & mount user SHARED dir, bind to vpnusers' SHARED dir
    mkdir "${USERHOME}"/SHARED
    mount --bind "${VPNDIR}"/SHARED "${USERHOME}"/SHARED
    echo "Mounted shared dir in ${USERHOME}."
    # Set user shared dir to auto-mount on boot
    echo ""${VPNDIR}"/SHARED "${USERHOME}"/SHARED none bind" >> /etc/fstab
    echo "Updated fstab to auto-mount shared dir on boot."
    
}

bye() {
    echo "Have a good day! :)"
}

main