#!/bin/bash
################################
# title: VM Resource Accounter #
# name: mithos                 #
# date: 8/30/17                #
# last updated: 11/8/17        #
# purpose: Display VM name,    #
#   CPU count, RAM size,       #
#   HDD size, and current      #
#   running state of VMs       #
# calls: vboxmanage, grep      #
# called by: user              #
################################

### Initialization ###

vboxmanage list vms > vmlist.tmp                        # List currently installed VMs
VMLIST="$(sed -r 's/"//g;s/.{38}$//' vmlist.tmp)"       # Strip " from VM names;remove last 38 chars of line, store in VMLIST var
echo $VMLIST > vmlist.tmp                               # Put list of VMs on single line, space-delimited, for use in array
VMCOUNT="$(echo $VMLIST | wc -w )"                      # Set VMCOUNT var to the count of installed VMs, for use in array
VMARRAY=($VMLIST)                                       # Create array of VM names
i="0"                                                   # init i

### Main ###

# while i is less than the count of VMs
while [ $i -lt "$VMCOUNT" ]
do
# Print VM name
vboxmanage showvminfo "${VMARRAY[$i]}" | egrep -m 1 "${VMARRAY[$i]}"
# Print VM CPU/RAM info
vboxmanage showvminfo "${VMARRAY[$i]}" | egrep 'Number of CPUs:|Memory size:|State:'
echo
# increment i
i=$[$i+1]
done
#woot woot, it works!

### Cleanup ###
rm ./vmlist.tmp