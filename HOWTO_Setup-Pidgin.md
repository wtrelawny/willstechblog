# How To: Install/Configure Pidgin Messenger

This guide walks you through how to install & configure Pidgin, the XMPP client of choice for chatting on WillNET VPN.

Technically, you could use any XMPP client you want, but Pidgin is the only one I know for sure works and is officially supported.

---

## Prerequisistes

1. Connection to WillNET VPN. (**_[See guide](HOWTO_Setup-VPN.md)_**)

---

### Installation

1. Download & Install [Pidgin](https://pidgin.im/download/windows/)

2. Download & Install [Pidgin OTR](https://otr.cypherpunks.ca/binaries/windows/pidgin-otr-4.0.2.exe). This enables "Off the Record" chat functionality that I've enabled on the server. Read about it [here](https://otr.cypherpunks.ca/) if you feel so inclined.

### Pidgin Client Configuration

1. Launch Pidgin. At the top banner, click **"Accounts" > "Manage Accounts"**, then click **"Add..."**.

2. In the **Basic** tab and enter the following information:

    - **Protocol:** _XMPP_

    - **Username:** _[choose any username you like]_

    - **Domain:** _conduit.lab.willstechblog.ddns.net_

    - **Local alias _(optional)_:** _[choose an alias that others will see you as]_

        **_Note:** if another user sets an alias for you when adding you, they won't see your alias._

    - **Use this buddy icon for this account _(optional)_:** _[set an icon for your account]_

    - Leave all other fields blank (**_including the Password field- you'll enter it later._**)

    ![Basic Tab.](media/pic9.png)

3. Go to the **Advanced** tab and enter the following information:

    - **Connect port:** _8443_

    - **Connect server:** _conduit.lab.willstechblog.ddns.net_

    - At the bottom, check the **"Create this new account on the server"** box. Click **Add**.

    ![Advanced Tab.](media/pic10.png)

4. Back in the **Accounts** window, check the **Enabled** box. Enter your **username** and **set a password**. The account is now activated!

    ![Enable Account.](media/pic11.png)

---

## Optional Configuration

### Add Buddies

1. In the Pidgin client, at the top banner menu, click **"Buddies" > "Add Buddy"**.

2. Enter the new Buddy's **username** and _optionally_ an **alias** for them. Make sure to append the **domain** to the Buddy's username, as in the image below:

    ![Add a Buddy!](media/pic12.png)

    - This will send an Authorization Request to the user, who will have to accept your request. Until then, the Buddy List will show "Not Authorized" under their name.

### Chat Groups/Chat Rooms

- To create a chat group, click **"Buddies" > "Add Group"** and give it a name. Then, right click on a Buddy, go to **"Move to"** and select the group.

- To create a chat room, click **"Buddies" > "Add Chat"**, and fill in the information as you see fit:

    - **Room:** Name the room

    - **Server:** **_LEAVE AS-IS_**

    - **Handle (_optional_):** Your user's handle within the room.

    - **Password (_optional_):** Set a password for the room.

    - **Alias (_optional; by default is the "Room" value_):** Make an alias for the room in your Buddy List.

    - **Group (_optional_):** Add chat room to a specific Group in your Buddy List.

    - Optionally check the two boxes at the end: "Automatically join when an account connects" & "Remain in chat after window is closed."

    ![Add Chat Room.](media/pic15.png)

---

## Important Notes

- There are a **TON** of ways to customize the Pidgin client, so feel free to explore and experiment!

- Technically it is possible to create _multiple_ accounts on the same chat server, though there isn't much of a reason to. Just follow the same steps as above, and enable/disable your accounts as you wish.

---

## Other Guides

- **[How To: Set Up WillNET VPN File Sharing on Windows](HOWTO_Setup-FileSharing.md)**